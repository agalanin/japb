#!/bin/sh
############################################################################
#
# Just Another Package Builder
#
# Build script wrapper
#
# $Id$
#
############################################################################
# \
exec tclsh8.5 "$0" "$@"

package require Tcl 8.5

lappend auto_path [file normalize [file join [file dirname [info script]] lib]]

package require japb 1.0
package require japb::filenode 1.0

if {[llength $argv] == 0} {
    usage
    exit 1
}

set scriptName [lindex $argv 0]
set argv [lrange $argv 1 end]

namespace eval ::japb::currentScript {
    namespace import ::japb::*
}

namespace eval ::japb::currentScript [list \
    source $scriptName
]
